let gulp = require('gulp');
let sass = require('gulp-sass');
let minifyCSS = require('gulp-clean-css');
let uglifyJS = require('gulp-jsmin');

gulp.task('styles', () => {
      return gulp.src('./src/scss/*.scss')
            .pipe(sass())
            .pipe(minifyCSS())
            .pipe(gulp.dest('./dist/css/'));
});

gulp.task('scripts', () => {
      return gulp.src('src/js/*.js')
            .pipe(uglifyJS())
            .pipe(gulp.dest('dist/js/'));
});

gulp.task('images', () => {
      return gulp.src('src/img/*')
            .pipe(gulp.dest('dist/img/'));
});

gulp.task('build', gulp.series('images', 'styles', 'scripts'));



